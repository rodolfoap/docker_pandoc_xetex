# docker_pandoc_xetex

Simple packing of pandoc + latex + xetex

Dockerfile: https://gitlab.com/rodolfoap/docker_pandoc_xetex

### Usage
```
docker run --rm -v $(pwd):/home/node rodolfoap/pandoc_xetex:$TAG SourceFile.md --pdf-engine=xelatex -c /usr/local/lib/mvp.css -H /usr/local/lib/deeplists.tex --metadata title="${TITLE}" -o TargetFile.html
```
