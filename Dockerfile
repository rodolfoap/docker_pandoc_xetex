FROM pandoc/latex
RUN apk update && apk add texlive-full
RUN tlmgr install enumitem && texhash
